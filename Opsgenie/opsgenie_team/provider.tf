terraform {
  # https://www.terraform.io/docs/configuration/terraform.html#specifying-a-required-terraform-version
  required_version = "~> 0.13"

  # https://www.terraform.io/docs/configuration/provider-requirements.html
  required_providers {
    opsgenie = {
      source  = "opsgenie/opsgenie"
      version = "~> 0"
    }
  }
}

provider "opsgenie" {}
