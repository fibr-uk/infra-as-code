output "team_id" {
  value = opsgenie_team.team.id
}

output "team_name" {
  value = opsgenie_team.team.name
}

# output "sentry_api_key" {
#   value = opsgenie_api_integration.sentry.api_key
# }

# output "datadog_api_key" {
#   value = opsgenie_api_integration.datadog.api_key
# }