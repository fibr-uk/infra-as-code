variable "team_name" {
  description = "Team name"
  type = string
}

variable "team_description" {
  description = "Team description"
  type = string
}

variable "team_members_ids" {
  description = "map of ids and roles of team members"
  type = list(map(any))
}

variable "lvl2_schedule_name" {
  description = "name of lvl2 schedule"
  type = string
  default = "Tech Leads Schedule"
}