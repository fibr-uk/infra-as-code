data "opsgenie_schedule" "lead_schedule" {
  name = var.lvl2_schedule_name
}

resource "opsgenie_team" "team" {
  name           = format("%s Team",var.team_name)
  description    = var.team_description
  delete_default_resources = true

  dynamic "member"{
    for_each = var.team_members_ids
    content{
      role = member.value.role
      id = member.value.id
    }
  }
}

# resource "opsgenie_api_integration" "datadog" {
#   name = format("%s Datadog integration",var.team_name)
#   owner_team_id = opsgenie_team.team.id
#   type = "Datadog"
# }

# resource "opsgenie_api_integration" "sentry" {
#   name = format("%s Sentry integration",var.team_name)
#   owner_team_id = opsgenie_team.team.id
#   type = "Sentry"
# }

resource "opsgenie_team_routing_rule" "routing_rule" {
  name     = format("%s Routing rule",var.team_name)
  team_id  = opsgenie_team.team.id
  order    = 0
  timezone = "Europe/Amsterdam"
  criteria {
    type = "match-any-condition"
    conditions {
        field = "priority"
        not =  true
        operation = "equals"
        expected_value = "P5"
    }
  }
  notify {
    type = "escalation"
    id = opsgenie_escalation.escalation.id
  }
}

resource "opsgenie_escalation" "escalation" {
  name = format("%s Escalation",var.team_name)
  owner_team_id = opsgenie_team.team.id

  rules {
    condition   = "if-not-acked"
    notify_type = "default"
    delay       = 0

    recipient {
      type = "schedule"
      id   = opsgenie_schedule.schedule.id
    }
  }

  rules {
    condition   = "if-not-acked"
    notify_type = "default"
    delay       = 10

    recipient {
      type = "schedule"
      id   = data.opsgenie_schedule.lead_schedule.id
    }
  }
}

resource "opsgenie_schedule" "schedule" {
  name          = format("%s 24-7 Schedule",var.team_name)
  description   = format("24-7 schedule for %s team", var.team_name)
  timezone      = "Europe/Amsterdam"
  enabled       = true
  owner_team_id = opsgenie_team.team.id
}

resource "opsgenie_schedule_rotation" "rotation" { 
  schedule_id = opsgenie_schedule.schedule.id
  name        = format("%s Rotation",var.team_name)
  start_date  = "2021-03-17T09:00:00Z"
  type        = "weekly"

  dynamic "participant"{
    for_each = var.team_members_ids
    content{
      type = "user"
      id = participant.value.id
    }
  }
}

resource "opsgenie_team_routing_rule" "workday_routing_rule" {
  name     = format("%s workday Routing rule",var.team_name)
  team_id  = opsgenie_team.team.id
  order    = 0
  timezone = "Europe/Amsterdam"
  criteria {
    type = "match-any-condition"
    conditions {
        field = "priority"
        not =  false
        operation = "equals"
        expected_value = "P5"
    }
  }
  notify {
    type = "schedule"
    id = opsgenie_schedule.workday_schedule.id
  }
}

resource "opsgenie_schedule" "workday_schedule" {
  name          = format("%s workday Schedule",var.team_name)
  description   = format("workday schedule for %s team", var.team_name)
  timezone      = "Europe/Amsterdam"
  enabled       = true
  owner_team_id = opsgenie_team.team.id
}

resource "opsgenie_schedule_rotation" "workday_rotation" { 
  schedule_id = opsgenie_schedule.workday_schedule.id
  name        = format("%s workday Rotation",var.team_name)
  start_date  = "2021-03-17T09:00:00Z"
  type        = "weekly"

  dynamic "participant"{
    for_each = var.team_members_ids
    content{
      type = "user"
      id = participant.value.id
    }
  }
  time_restriction {
    type = "weekday-and-time-of-day"
    restrictions {
      start_day  = "monday"
      start_hour = 9
      start_min  = 0
      end_day    = "monday"
      end_hour   = 17
      end_min    = 0
    }
    restrictions {
      start_day  = "tuesday"
      start_hour = 9
      start_min  = 0
      end_day    = "tuesday"
      end_hour   = 17
      end_min    = 0
    }
    restrictions {
      start_day  = "wednesday"
      start_hour = 9
      start_min  = 0
      end_day    = "wednesday"
      end_hour   = 17
      end_min    = 0
    }
    restrictions {
      start_day  = "thursday"
      start_hour = 9
      start_min  = 0
      end_day    = "thursday"
      end_hour   = 17
      end_min    = 0
    }
    restrictions {
      start_day  = "friday"
      start_hour = 9
      start_min  = 0
      end_day    = "friday"
      end_hour   = 17
      end_min    = 0
    }
  }
}