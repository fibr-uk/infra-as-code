data "opsgenie_team" "Delta" {
  name = "Delta Squad"
}
data "opsgenie_team" "Zion" {
  name = "Delta Squad"
}
data "opsgenie_team" "PB" {
  name = "Patatas Bravas Squad"
}

#Patatas Bravas services
module "user_application" {
  source = "../opsgenie_service"
  service_name = "user-application"
  service_description = "User application process up until Firm Offer via the User Interface"
  team_id = data.opsgenie_team.PB.id
  team_name = data.opsgenie_team.PB.name
}

module "broker_user_application" {
  source = "../opsgenie_service"
  service_name = "broker-user-application"
  service_description = "Application of new customers via a broker up until Firm Offer via the User Interface"
  team_id = data.opsgenie_team.PB.id
  team_name = data.opsgenie_team.PB.name
}

module "api_user_application" {
  source = "../opsgenie_service"
  service_name = "api-user-application"
  service_description = "Application of new customers via the public API up until Firm Offer via the User Interface"
  team_id = data.opsgenie_team.PB.id
  team_name = data.opsgenie_team.PB.name
}


#Delta services
module "user_onboarding_FLOC" {
  source = "../opsgenie_service"
  service_name = "user-onboarding-FLOC"
  service_description = "User onboarding process post firm offer up until first loan is issued for our Flexible Line Of Credit"
  team_id = data.opsgenie_team.Delta.id
  team_name = data.opsgenie_team.Delta.name
}

module "user_servicing_FLOC" {
  source = "../opsgenie_service"
  service_name = "user-servicing-FLOC"
  service_description = "Servicing of all users needs once originally onboarded on our dashboard for our Flexible Line Of Credit product"
  team_id = data.opsgenie_team.Delta.id
  team_name = data.opsgenie_team.Delta.name
}

module "user_onboarding_TL" {
  source = "../opsgenie_service"
  service_name = "user-onboarding-TL"
  service_description = "User onboarding process post firm offer up until first loan is issued for our Term Loan product"
  team_id = data.opsgenie_team.Delta.id
  team_name = data.opsgenie_team.Delta.name
}

module "user_servicing_TL" {
  source = "../opsgenie_service"
  service_name = "user-servicing-TL"
  service_description = "Servicing of all users needs once originally onboarded on our dashboard for our Term Loan product"
  team_id = data.opsgenie_team.Delta.id
  team_name = data.opsgenie_team.Delta.name
}

#Zion services
module "admin_panel_CRM_legacy" {
  source = "../opsgenie_service"
  service_name = "admin-panel-CRM-legacy"
  service_description = "All admin panel functionalities covering its CRM abilities - Legacy"
  team_id = data.opsgenie_team.Zion.id
  team_name = data.opsgenie_team.Zion.name
}

module "admin_panel_platform_settings" {
  source = "../opsgenie_service"
  service_name = "admin-panel-platform-settings"
  service_description = "Modification of the setting of the platform via the Admin Panl (self servicing of rules, risk models, etc.)"
  team_id = data.opsgenie_team.Zion.id
  team_name = data.opsgenie_team.Zion.name
}

module "salesforce_data_interaction" {
  source = "../opsgenie_service"
  service_name = "salesforce-data-interaction"
  service_description = "All Salesforce integrations with the platform data, both read and write"
  team_id = data.opsgenie_team.Zion.id
  team_name = data.opsgenie_team.Zion.name
}

module "salesforce_communication_channels" {
  source = "../opsgenie_service"
  service_name = "salesforce-communication-channels"
  service_description = "Voice, chat ane email communication channels in Salesfoce and the integration with the providers"
  team_id = data.opsgenie_team.Zion.id
  team_name = data.opsgenie_team.Zion.name
}

