# Global alert policies
resource "opsgenie_alert_policy" "alert_policy_non_prd" {
  name               = "1- Set P5"
  policy_description = "known low-env alerts are P5"
  message            = "{{message}}"
  priority           = "P5"
  continue_policy    = false
  filter {
    type = "match-any-condition"
    conditions {
        field = "extra-properties"
        key = "Datadog Tags"
        not =  false
        operation = "contains"
        expected_value = "dev"
    }
    conditions {
        field = "extra-properties"
        key = "Environment"
        not =  false
        operation = "contains"
        expected_value = "dev"
    }
    conditions {
        field = "extra-properties"
        key = "Datadog Tags"
        not =  false
        operation = "contains"
        expected_value = "staging"
    }
    conditions {
        field = "extra-properties"
        key = "Environment"
        not =  false
        operation = "contains"
        expected_value = "staging"
    }
  }
}
resource "opsgenie_alert_policy" "alert_policy_catch_all" {
  name               = "0- Set P2"
  policy_description = "all alerts are P2"
  message            = "{{message}}"
  priority           = "P2"
  continue_policy    = true
  filter {
    type = "match-any-condition"
    conditions {
        field = "source"
        not =  false
        operation = "equals"
        expected_value = "Datadog"
    }
    conditions {
        field = "source"
        not =  false
        operation = "equals"
        expected_value = "Sentry"
    }
    conditions {
        field = "source"
        not =  false
        operation = "equals"
        expected_value = "Jira Service Desk"
    }
  }
}

## create team
resource "opsgenie_team" "TLteam" {
  name           = "Tech Leads"
  description    = "Tech Leads Team"
  delete_default_resources = true

  dynamic "member"{
    for_each = local.team_members
    content{
      role = member.value.role
      id = member.value.id
    }
  }
}
## create a simple schedule
resource "opsgenie_schedule" "TLschedule" {
  name          = "Tech Leads Schedule"
  description   = "generic schedule for %Tech Lead team"
  timezone      = "Europe/London"
  enabled       = true
  owner_team_id = opsgenie_team.TLteam.id
}
## and a simple roatation
resource "opsgenie_schedule_rotation" "TLrotation" { 
  schedule_id = opsgenie_schedule.TLschedule.id
  name        = "Tech Leads Rotation"
  start_date  = "2021-04-09T09:00:00Z"
  type        = "weekly"

  dynamic "participant"{
    for_each = local.team_members
    content{
      type = "user"
      id = participant.value.id
    }
  }
}
## a simple escaltion (only one level)
resource "opsgenie_escalation" "TLescalation" {
  name = "Tech Leads Escalation"
  owner_team_id = opsgenie_team.TLteam.id

  rules {
    condition   = "if-not-acked"
    notify_type = "default"
    delay       = 0

    recipient {
      type = "schedule"
      id   = opsgenie_schedule.TLschedule.id
    }
  }
}
##and a catch-all routing rule
resource "opsgenie_team_routing_rule" "TLrouting_rule" {
  name     = "Tech Leads Routing rule"
  team_id  = opsgenie_team.TLteam.id
  order    = 0
  timezone = "Europe/Amsterdam"
  criteria {
    type = "match-all"
  }
  notify {
    type = "escalation"
    id = opsgenie_escalation.TLescalation.id
  }
}

# # integrations, just in case
# resource "opsgenie_api_integration" "TLdatadog" {
#   name = "Tech Leads Datadog integration"
#   owner_team_id = opsgenie_team.TLteam.id
#   type = "Datadog"
# }

# resource "opsgenie_api_integration" "TLsentry" {
#   name = "Tech Leads Sentry integration"
#   owner_team_id = opsgenie_team.TLteam.id
#   type = "Sentry"
# }