terraform {
  # https://www.terraform.io/docs/configuration/terraform.html#specifying-a-required-terraform-version
  required_version = "~> 0.12"

  # https://www.terraform.io/docs/configuration/provider-requirements.html
  required_providers {
    opsgenie = {
      source  = "opsgenie/opsgenie",
      version = "~> 0"
    }
  }
}

provider "opsgenie" {
  api_key = "c7ae598b-f6d8-4d65-99dc-87d690d06a8f"
  api_url = "api.eu.opsgenie.com"
}
