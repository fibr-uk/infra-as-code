variable "service_name" {
  description = "Service name"
  type = string
}

variable "service_description" {
  description = "Service description"
  type = string
}

variable "team_id" {
  description = "id of the team owning the service"
  type = string
}

variable "team_name" {
  description = "name of the team owning the service"
  type = string
}
