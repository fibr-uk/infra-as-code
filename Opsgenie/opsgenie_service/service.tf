resource "opsgenie_service" "service" {
  name  = var.service_name
  description = var.service_description
  team_id = var.team_id
}

# resource "opsgenie_service_incident_rule" "prd-datadog-rule" {
#   service_id = opsgenie_service.service.id
#   incident_rule {
#     condition_match_type = "match-all-conditions"
#     conditions {
#         field = "extra-properties"
#         key = "Datadog Tags"
#         not =  false
#         operation = "contains"
#         expected_value = var.service_name
#     }
#     conditions {
#         field = "priority"
#         not =  true
#         operation = "equals"
#         expected_value = "P5"
#     }
#     incident_properties {
#         message = format("%s INCIDENT ON PRODUCTION.", var.service_name)
#         tags = [var.service_name, var.team_name, "prd"]
#         priority = "P2"
#         stakeholder_properties {
#             message = format("Service is disrupted on %s. %s has been alerted.", var.service_name, var.team_name)
#             enable = "true"
#         }
#     }
#   }
# }
# resource "opsgenie_service_incident_rule" "prd-sentry-rule" {
#   service_id = opsgenie_service.service.id
#   incident_rule {
#     condition_match_type = "match-all-conditions"
#     conditions {
#         field = "extra-properties"
#         key = "Project ID"
#         not =  false
#         operation = "contains"
#         expected_value = var.service_name
#     }
#     conditions {
#         field = "priority"
#         not =  true
#         operation = "equals"
#         expected_value = "P5"
#     }
#     incident_properties {
#         message = format("%s INCIDENT ON PRODUCTION.", var.service_name)
#         tags = [var.service_name, var.team_name, "prd"]
#         priority = "P2"
#         stakeholder_properties {
#             message = format("Service is disrupted on %s. %s has been alerted.", var.service_name, var.team_name)
#             enable = "true"
#         }
#     }
#   }
# }
resource "opsgenie_service_incident_rule" "prd-jsd-rule" {
  service_id = opsgenie_service.service.id
  incident_rule {
    condition_match_type = "match-all-conditions"
    conditions {
        field = "extra-properties"
        key = "Project ID"
        not =  false
        operation = "contains"
        expected_value = var.service_name
    }
    conditions {
        field = "priority"
        not =  true
        operation = "equals"
        expected_value = "P5"
    }
    incident_properties {
        message = format("%s INCIDENT ON PRODUCTION.", var.service_name)
        tags = [var.service_name, var.team_name, "prd"]
        priority = "P2"
        stakeholder_properties {
            message = format("Service is disrupted on %s. %s has been alerted.", var.service_name, var.team_name)
            enable = "true"
        }
    }
  }
}