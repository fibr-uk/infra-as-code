output "service_id" {
  value = opsgenie_service.service.id
}

output "service_name" {
  value = opsgenie_service.service.name
}
