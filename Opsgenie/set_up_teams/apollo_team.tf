data "opsgenie_user" "Alexander" {
  username = "a.degoeij@atbank.io"
}
data "opsgenie_user" "Ruben" {
  username = "r.vosmeer@atbank.nl"
}
data "opsgenie_user" "Alex" {
  username = "a.matura@atbank.nl"
}

module "apollo" {
  source = "../opsgenie_team"
  team_name = "Apollo"
  team_description = "Team owning the payments systems"
  team_members_ids = [
    {
      id = data.opsgenie_user.Alexander.id
      role = "user"
    },
    {
      id = data.opsgenie_user.Ruben.id
      role = "admin"
    },
    {
      id = data.opsgenie_user.Alex.id
      role = "user"
    }
  ]
}